def cube(n):
    for i in range(n):
        yield i**3

def fib(n):
    a, b = 0, 1
    for i in range(n):
        yield a
        a, b = b, a + b

def twopn(n):
    for i in range(n):
        yield 2**i


for i in cube(10):
    print i,
print ""

for i in fib(10):
    print i,
print ""

for i in twopn(10):
    print i,
