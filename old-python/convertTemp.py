print "Choose conversion:"
print "1 = C -> F"
print "2 = F -> C"
choice = int(raw_input("Enter 1 or 2: "))

inp = int(raw_input("Enter temperature: "))

if choice == 1:
    f = inp *  9/5 + 32
    print "Temperature in Fahrenheit is", str(f) + "\xba"
elif choice == 2:
    c = (inp - 32) * 5/9
    print "Temperature in Celsius is", str(c) + "\xba"
else:
    print "Invalid choice"