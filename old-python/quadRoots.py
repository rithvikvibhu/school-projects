a = float(raw_input("Enter coefficient of x^2: "))
b = float(raw_input("Enter coefficient of x: "))
c = float(raw_input("Enter constant: "))

try:
    x1 = (-b + (b**2 - 4*a*c)**(0.5) ) / 2*a
    x2 = (-b - (b**2 - 4*a*c)**(0.5) ) / 2*a
    print x1
    print x2
except ValueError:
    print "No solution to equation"
