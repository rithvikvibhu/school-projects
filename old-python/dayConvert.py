days = int(raw_input("Enter number of days: "))

if days >= 365:
    years = days // 365
    days = days % 365

if days >= 30:
    months = days // 30
    days = days % 30

if days >= 7:
    weeks = days // 7
    days = days % 7

print years, "Years,", months, "Months,", weeks, "Weeks, and", days, "Days"