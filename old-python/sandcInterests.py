print "Choose type of interest:"
print "1 = Simple Interest"
print "2 = Compound Interest"
choice = int(raw_input("Enter 1 or 2: "))

principal = float(raw_input("Enter principal: "))
rate = float(raw_input("Enter rate of interest in %: ").strip("%"))
time = float(raw_input("Enter time in years: "))

if choice == 1:
    si = principal * rate/100 * time
    print "Simple Interest is", si
elif choice == 2:
    ci = ( principal * (1 + (rate/100))**(time) ) - principal
    print "Compound Interest is", ci
else:
    print "Invalid choice."