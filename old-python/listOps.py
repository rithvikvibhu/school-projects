inp = []

def printList(clear=False, text=""):
    if clear:
        print "\n"*15
        print "Enter letter to perform operation on list:"
        print "a = Add element\nm = Modify element\nr = Remove element\ns = Sort list\nd = Display list\ne = Exit"
    print text, inp

while True:
    inpstr = raw_input("Enter element: ")
    if inpstr == "":
        break
    inp.append(inpstr)

printList(True, "Your list: ")

while True:
    operation = raw_input("Enter operation: ")
    if operation.lower() == "a":
        inp.append(raw_input("Enter element to append: "))
        printList(True, "Your new list: ")
    elif operation.lower() == "r":
        inp.remove(raw_input("Enter element to remove: "))
        printList(True, "Your new list: ")
    elif operation.lower() == "m":
        inp[int(raw_input("Enter index of element to modify: "))] = raw_input("Enter new element: ")
        printList(True, "Your new list: ")
    elif operation.lower() == "s":
        inp.sort()
        printList(True, "Your new list: ")
    elif operation.lower() == "d":
        printList(True, "Your list: ")
    elif operation.lower() == "e":
        break
    else:
        printList(True, "Invalid Option")