num = float(raw_input("Enter number: "))

if num % 2 == 0 and num != 0:
    print "Number is even"
elif num == 0:
    print "0 is neither even nor odd"
else:
    print "Number is odd"