num = int(raw_input("Enter number: "))
prime_count = 0

for a in xrange(1,num+1):
    for b in xrange(1, num+1):
        if a*b == num:
            prime_count += 1

if prime_count > 2:
    print "Number not prime"
else:
    print "Number is prime"