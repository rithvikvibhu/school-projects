# teams = [...team]
# team = {name: "Name", wins: 0, loses: 0}

teams = []

def printText(clear=False, text=""):
    if clear:
        print "\n"*15
        print "Enter letter to perform task:"
        print '''
        a = Calculate win percentage for specific team
        b = List wins of all teams
        c = Some confusing thing
        e = Exit'''
    print "\n" + text + "\n"

while True:
    name = raw_input("Enter Team name: ")
    if name == "":
        break
    teams.append({'name': name, 'wins': int(raw_input("Enter team wins: ")), 'loses': int(raw_input("Enter team loses: "))})
printText(True)
while True:
    op = raw_input("Enter letter: ").lower()
    if op == "a":
        nameinp = raw_input("Enter team name: ")
        for team in teams:
            if team["name"] == nameinp:
                printText(True, "Win percentage of team: " + str(float(team["wins"])/float(team["wins"]+team["loses"])*100) + " %")
                break
            else:
                printText(True, "Team not found")
    elif op == "b":
        winlist = []
        for team in teams:
            winlist.append(team["wins"])
        printText(True, "Wins list :"+str(winlist))
    elif op == "c":
        winrecord = []
        for team in teams:
            if team["wins"] >= 1:
                winrecord.append(team["name"])
        printText(True, "Teams with winning record: " + str(winrecord))
    elif op.lower() == "e":
        break
    else:
        printText(True, "Invalid Option")