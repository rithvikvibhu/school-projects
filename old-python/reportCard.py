print "Use this format: Physics Chemistry Math"
print "Example: 83 92 65"
p1,c1,m1 = raw_input("Enter Half-yearly exam marks   : ").split(" ")
p2,c2,m2 = raw_input("Enter Final exam marks         : " ).split(" ")

print "\n\n\n\n\n\n"#-----------------------"
print "REPORT CARD (2016-2017)"
print "-----------------------"

print "Name: Test\t\t\tClass: XI\n"
print "--------------------------"
print "|Subject\t | Grade |"
print "--------------------------"

# Physics
if (int(p1) + int(p2)) > 180:
    print " PHYSICS\t\t | A1"
elif (int(p1) + int(p2)) > 160:
    print " PHYSICS\t\t | A2"
elif (int(p1) + int(p2)) > 140:
    print " PHYSICS\t\t | B1"
elif (int(p1) + int(p2)) > 120:
    print " PHYSICS\t\t | B2"
else:
    print " PHYSICS\t\t | F"

# Chemistry
if (int(c1) + int(c2)) > 180:
    print " CHEMISTRY\t | A1"
elif (int(c1) + int(c2)) > 160:
    print " CHEMISTRY\t | A2"
elif (int(c1) + int(c2)) > 140:
    print " CHEMISTRY\t | B1"
elif (int(c1) + int(c2)) > 120:
    print " CHEMISTRY\t | B2"
else:
    print " CHEMISTRY\t | F"

# Math
if (int(m1) + int(m2)) > 180:
    print " MATH\t\t | A1"
elif (int(m1) + int(m2)) > 160:
    print " MATH\t\t | A2"
elif (int(m1) + int(m2)) > 140:
    print " MATH\t\t | B1"
elif (int(m1) + int(m2)) > 120:
    print " MATH\t\t | B2"
else:
    print " MATH\t\t | F"

total = int(p1) + int(p2) + int(c1) + int(c2) + int(m1) + int(m2)
if total > 540:
    grade = "A1"
elif total > 480:
    grade = "A2"
elif total > 420:
    grade = "B1"
elif total > 360:
    grade = "B2"
else:
    grade = "F"
print "\nOverall Grade: " + grade