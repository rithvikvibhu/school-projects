string = str(raw_input("Enter string: "))

if string.isdigit():
    print "String is number"
elif string.isalpha():
    print "String is letter"
elif string.isalnum():
    print "String is alphanumeric"

if string.isupper():
    print "String is UPPER CASE"
if string.islower():
    print "String is lower case"
if not string.isupper() and not string.islower() and not string.isdigit():
    print "String is MixED CasE"
