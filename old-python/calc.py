choice = raw_input("Choose operation: + or - or * or /: ")
a = float(raw_input("Enter 1st number: "))
b = float(raw_input("Enter 2nd number: "))

if choice == "+":
    print "Sum of numbers is", a+b
elif choice == "-":
    print "Difference of numbers is", a-b
elif choice == "/":
    print "Quotient is", a/b, "and remainder is", a//b
elif choice == "*":
    print "Product of numbers is", a*b
else:
    print "Invalid choice"